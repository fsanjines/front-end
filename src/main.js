import Vue from 'vue'
import App from './App.vue'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css' //Vuesax styles


//import Service from './plugins/Service';
//Vue.use(Service);

import Storage from './plugins/Storage';
Vue.use(Storage);

Vue.use(Vuesax, {
  theme:{
    colors:{
      primary:'#5b3cc4',
      success:'rgb(23, 201, 100)',
      danger:'rgb(242, 19, 93)',
      warning:'rgb(255, 130, 0)',
      dark:'rgb(36, 33, 69)'
    }
  }
})


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
